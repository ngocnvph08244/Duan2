package com.project.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "product")
@NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "product_id", unique = true)
	private String productId;

	private Long amount;

	@Column(nullable = false)
	private String code;

	private String color;

	@CreationTimestamp
	@Column(name = "created_date", updatable = false, nullable = true)
	private Timestamp createdDate;

	@Column(name = "last_modify_by")
	private String lastModifyBy;

	@UpdateTimestamp
	@Column(name = "last_modify_date", updatable = false, nullable = true)
	private Timestamp lastModifyDate;

	private Double mass;

	@Column(nullable = false, length = 200)
	private String name;

	private String painting;

	private String enamel;

	@Column(nullable = false)
	private Long price;

	@Column(name = "is_delete", nullable = false)
	private Long isDelete;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private Category category;

	@ManyToOne
	@JoinColumn(name = "supplier_id")
	private Supplier supplier;

	public Product() {
	}

	public String getProductId() {
		return this.productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public String getLastModifyBy() {
		return this.lastModifyBy;
	}

	public Timestamp getLastModifyDate() {
		return this.lastModifyDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPainting() {
		return this.painting;
	}

	public void setPainting(String painting) {
		this.painting = painting;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Double getMass() {
		return mass;
	}

	public void setMass(Double mass) {
		this.mass = mass;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getEnamel() {
		return enamel;
	}

	public void setEnamel(String enamel) {
		this.enamel = enamel;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [productId=");
		builder.append(productId);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", code=");
		builder.append(code);
		builder.append(", color=");
		builder.append(color);
		builder.append(", createdDate=");
		builder.append(createdDate);
		builder.append(", lastModifyBy=");
		builder.append(lastModifyBy);
		builder.append(", lastModifyDate=");
		builder.append(lastModifyDate);
		builder.append(", mass=");
		builder.append(mass);
		builder.append(", name=");
		builder.append(name);
		builder.append(", painting=");
		builder.append(painting);
		builder.append(", enamel=");
		builder.append(enamel);
		builder.append(", price=");
		builder.append(price);
		builder.append(", category=");
		builder.append(category);
		builder.append(", supplier=");
		builder.append(supplier);
		builder.append("]");
		return builder.toString();
	}

}