package com.project.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.sql.Timestamp;

/**
 * The persistent class for the account database table.
 * 
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "account")
@NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a")
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique = true, nullable = false, length = 32)
	private String username;

//	@CreatedBy
	@Column(name = "created_by", nullable = true)
	private String createdBy;

	@CreationTimestamp
	@Column(name = "created_date", nullable = true)
	private Timestamp createdDate;

	@Column(nullable = false)
	private Long isDelete;

	@UpdateTimestamp
	@LastModifiedDate
	@Column(name = "last_modify_date", nullable = true)
	private Timestamp lastModifyDate;

	@Column(nullable = false, length = 60)
	private String password;

	@Column(nullable = false, length = 25)
	private String role;

	public Account() {
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public Long getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Long isDelete) {
		this.isDelete = isDelete;
	}

	public Timestamp getLastModifyDate() {
		return this.lastModifyDate;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}