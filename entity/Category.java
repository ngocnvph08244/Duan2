package com.project.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * The persistent class for the category database table.
 * 
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "category")
@NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "category_id", unique = true, nullable = false, length = 40)
	private String categoryId;

	@Column(length = 20)
	private String code;

	private Long isDelete;

	@Column(length = 25)
	private String name;

	@CreationTimestamp
	@Column(name = "created_date", updatable = false, nullable = true)
	private Timestamp createdDate;

	@UpdateTimestamp
	@Column(name = "last_modify_date", updatable = false, nullable = true)
	private Timestamp lastModifyDate;

	public Category() {
	}

	public String getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Long isDelete) {
		this.isDelete = isDelete;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getCreatedDate() {
		if (createdDate == null) {
			return null;
		}
		return new Timestamp(createdDate.getTime());
	}

	public Timestamp getLastModifyDate() {
		return lastModifyDate;
	}

}