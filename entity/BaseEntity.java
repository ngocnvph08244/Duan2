package com.project.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EntityListeners;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

	@CreationTimestamp
	@Column(name = "created_date", updatable = false)
	private Timestamp createdDate;

	@UpdateTimestamp
	@Column(name = "last_modify_date", updatable = false)
	private Timestamp lastModifyDate;
}
