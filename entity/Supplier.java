package com.project.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * The persistent class for the supplier database table.
 * 
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "supplier")
@NamedQuery(name = "Supplier.findAll", query = "SELECT s FROM Supplier s")
public class Supplier implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "supplier_id", unique = true, nullable = false, length = 40)
	private String supplierId;

	private String address;

	private String code;

	@Column(name = "created_by", updatable = false, nullable = true)
	private String createdBy;

	@CreationTimestamp
	@Column(name = "created_date", updatable = false, nullable = true)
	private Timestamp createdDate;

	@Column(length = 45)
	private String email;

	private Long isDelete;

	@Column(name = "last_modify_by", updatable = false, nullable = true)
	private String lastModifyBy;

	@UpdateTimestamp
	@Column(name = "last_modify_date", updatable = false, nullable = true)
	private Timestamp lastModifyDate;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String phone;

	@Column(nullable = false)
	private String represent;

	public Supplier() {
	}

	public String getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Long isDelete) {
		this.isDelete = isDelete;
	}

	public String getLastModifyBy() {
		return this.lastModifyBy;
	}

	public Timestamp getLastModifyDate() {
		return this.lastModifyDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRepresent() {
		return this.represent;
	}

	public void setRepresent(String represent) {
		this.represent = represent;
	}

}