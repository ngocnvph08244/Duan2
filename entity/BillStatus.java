package com.project.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * The persistent class for the bill_status database table.
 * 
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "bill_status")
@NamedQuery(name = "BillStatus.findAll", query = "SELECT b FROM BillStatus b")
public class BillStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(unique = true, nullable = false)
	private String id;

	@Column(name = "created_by", nullable = true)
	private String createdBy;

	@CreationTimestamp
	@Column(name = "created_date", updatable = false, nullable = true)
	private Timestamp createdDate;

	@Column(name = "finished_date")
	private Timestamp finishedDate;

	@Column(nullable = false)
	private Long status;

	@ManyToOne
	@JoinColumn(name = "bill_id")
	private Bill bill;

	public BillStatus() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public Timestamp getFinishedDate() {
		return this.finishedDate;
	}

	public void setFinishedDate(Timestamp finishedDate) {
		this.finishedDate = finishedDate;
	}

	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

}